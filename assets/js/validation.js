function counterDisplay(input, id, required) {
  var value = input.value.length;
  $('#' + id + '-counter-display').text(value);

  if (required) {
    if (value > 0) {
      document.getElementById(id + "-counter-tag").className = "tag is-success";
    } else {
      document.getElementById(id + "-counter-tag").className = "tag is-danger";
    }
  }
};